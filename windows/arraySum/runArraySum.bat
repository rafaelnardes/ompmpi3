del saidasMPI3\saida*
del saidasOMP\saida*

set I_MPI_PIN_PROCS=
FOR %%p IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (6881280 13762560 27525120 55050240 110100480) DO (

		mpiexec -n %%p arraySumMPI3.exe %%p %%s >> saidasMPI3\saidaAppendMPI3NoPinning.out
		@timeout 2
	)
	ECHO. >> saidasMPI3\saidaAppendMPI3NoPinning.out
)

set I_MPI_PIN_PROCS=allcores
FOR %%p IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (6881280 13762560 27525120 55050240 110100480) DO (

		mpiexec -n %%p arraySumMPI3.exe %%p %%s >> saidasMPI3\saidaAppendMPI3Allcores.out
		@timeout 2
	)
	ECHO. >> saidasMPI3\saidaAppendMPI3Allcores.out
)

set I_MPI_PIN_PROCESSOR_LIST=0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15
FOR %%p IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (6881280 13762560 27525120 55050240 110100480) DO (

		mpiexec -n %%p arraySumMPI3.exe %%p %%s >> saidasMPI3\saidaAppendMPI3Scatter.out
		@timeout 2
	)
	ECHO. >> saidasMPI3\saidaAppendMPI3Scatter.out
)


@set KMP_AFFINITY=compact

FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (6881280 13762560 27525120 55050240 110100480) DO (
		arraySumNU.exe %%t %%s %%t >> saidasOMP\saidaAppendCompactNU.out
		@timeout 2
	) 
	ECHO. >> saidasOMP\saidaAppendCompactNU.out
)
FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (6881280 13762560 27525120 55050240 110100480) DO (
		arraySumAPSingleIP.exe %%t %%s %%t >> saidasOMP\saidaAppendCompactAPSingleIP.out
		@timeout 2
	)
	ECHO.>>	saidasOMP\saidaAppendCompactAPSingleIP.out
)
FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (110100480) DO (
		arraySumAPIP.exe %%t %%s %%t >> saidasOMP\saidaAppendCompactAPIP.out
		@timeout 2
	)
	ECHO.>>	saidasOMP\saidaAppendCompactAPIP.out
)
FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (110100480) DO (
		arraySumASIP.exe %%t %%s %%t >> saidasOMP\saidaAppendCompactASIP.out
		@timeout 2
	)
	ECHO.>>	saidasOMP\saidaAppendCompactASIP.out
)


@set KMP_AFFINITY=scatter

FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (6881280 13762560 27525120 55050240 110100480) DO (
		arraySumNU.exe %%t %%s %%t >> saidasOMP\saidaAppendScatterNU.out
		@timeout 2
	)
	ECHO.>>	saidasOMP\saidaAppendScatterNU.out
)
FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (6881280 13762560 27525120 55050240 110100480) DO (
		arraySumAPSingleIP.exe %%t %%s %%t >> saidasOMP\saidaAppendScatterAPSingleIP.out
		@timeout 2
	) 
	ECHO.>> saidasOMP\saidaAppendScatterAPSingleIP.out
)
FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (110100480) DO (
		arraySumAPIP.exe %%t %%s %%t >> saidasOMP\saidaAppendScatterAPIP.out
		@timeout 2
	)
	ECHO.>>	saidasOMP\saidaAppendScatterAPIP.out
)
FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (110100480) DO (
		arraySumASIP.exe %%t %%s %%t >> saidasOMP\saidaAppendScatterASIP.out
		@timeout 2
	)
	ECHO.>>	saidasOMP\saidaAppendScatterASIP.out
)
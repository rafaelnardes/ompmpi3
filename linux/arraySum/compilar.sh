rm *.exe
icpc -O3 -g arraySumNU.cpp -fopenmp -o arraySumNU.exe
icpc -O3 -g arraySumAPIP.cpp -fopenmp -o arraySumAPIP.exe
icpc -O3 -g arraySumASIP.cpp -fopenmp -o arraySumASIP.exe
icpc -O3 -g arraySumAPSingleIP.cpp -fopenmp -o arraySumAPSingleIP.exe
mpiicpc -O3 -g arraySumMPI3.cpp -o arraySumMPI3.exe

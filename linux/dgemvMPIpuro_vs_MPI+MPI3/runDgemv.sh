#!/bin/bash
rm saidas/*
export I_MPI_PIN_PROCESSOR_LIST="0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15"
for p in 1 2 4 6 8 10 12 14 16
do
  for s in 1680 3360 6720 13440
  do
    mpirun -n $p ./dgemvMPIpuro.exe $p $s >> saidas/saidaMPIpuro.out
    #@timeout 2
  done
  echo >> saidas/saidaMPIpuro.out
done
for p in 1 2 4 6 8 10 12 14 16
do
  for s in 1680 3360 6720 13440
  do
    mpirun -n $p ./dgemvMPI+MPI3.exe $p $s >> saidas/saidaMPI+MPI3.out
    #@timeout 2
  done
  echo >> saidas/saidaMPI+MPI3.out
done

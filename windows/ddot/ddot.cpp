#include <omp.h>
#include <windows.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;

int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); 
	long long int domSize     = vecSize/nDomains;
	double alpha = 3.141592;

	int num_threads = atoi(argv[3]);

	double** a = (double**) malloc(nDomains*sizeof(double*));
	double** b = (double**) malloc(nDomains*sizeof(double*));
		
	double* elapsedTimes = new double[ nDomains ];
	double* localSums = new double[ nDomains ];
	double dotp = 0.0;
	
	//Início da região paralela
	//shared(elapsedTimes)
	#pragma omp parallel num_threads(num_threads) reduction(+:dotp)
	{			
		const int threadId = omp_get_thread_num();

		double* dataA[nDomains];
		double* dataB[nDomains];		
		
		#pragma omp single copyprivate(dataA,dataB)
		for ( int dom = 0; dom < nDomains; ++dom )
		{
			dataA[dom] = (double*) malloc(domSize*sizeof(double));
			dataB[dom] = (double*) malloc(domSize*sizeof(double));			
		}
		
		std::fill_n( dataA[threadId], domSize, 2.0 );
		std::fill_n( dataB[threadId], domSize, 3.0 );
						
		double* domInA = dataA[threadId];
		double* domInB = dataB[threadId];
					
		LARGE_INTEGER frequency;
		LARGE_INTEGER t1, t2;

		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&t1);		
		
		localSums[threadId] = 0.0;
		double mySum;
		for(int i=0; i<200; i++){
			mySum=0.0;
			for( long long int elem=0; elem<domSize; elem++ ){				
				mySum += domInA[elem] * domInB[elem];												
			}			
		}
		localSums[threadId] = mySum;		
		dotp=mySum;
		
		QueryPerformanceCounter(&t2);	
		elapsedTimes[ threadId ] = static_cast<double>( t2.QuadPart - t1.QuadPart ) / static_cast<double>( frequency.QuadPart );	
		
		a[threadId] = domInA;
		b[threadId] = domInB;		
	}
	
	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(elapsedTimes[i] > max)
			max = elapsedTimes[i];		
	}

	std::wcout << max << ", ";
	printf(" Dot Product: %.4f\n", dotp);	
		
	delete [] elapsedTimes;
	
}
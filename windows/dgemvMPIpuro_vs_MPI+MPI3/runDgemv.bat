del saidasMPI\saida*

@set I_MPI_PIN_PROCESSOR_LIST=0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15
REM @set I_MPI_PIN_PROCESSOR_LIST=allcores

FOR %%p IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (1680 3360 6720 13440) DO (

		mpiexec -n %%p dgemvMPIpuro.exe %%p %%s >> E:\testes_MPI\dgemvMPIvsMPI+MPI3\saidasMPI\saidaMPIpuro.out
		@timeout 2
	)
	ECHO. >> E:\testes_MPI\dgemvMPIvsMPI+MPI3\saidasMPI\saidaMPIpuro.out
)

FOR %%p IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (1680 3360 6720 13440) DO (

		mpiexec -n %%p dgemvMPI+MPI3.exe %%p %%s >> E:\testes_MPI\dgemvMPIvsMPI+MPI3\saidasMPI\saidaMPI+MPI3.out
		@timeout 2
	)
	ECHO. >> E:\testes_MPI\dgemvMPIvsMPI+MPI3\saidasMPI\saidaMPI+MPI3.out
)

#!/bin/bash
rm saidasMPI3/*
rm saidasOMP/*
export I_MPI_PIN_PROCESSOR_LIST="0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15"
for p in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    mpirun -n $p ./daxpyMPI3.exe $p $s >> saidasMPI3/saidaMPI3.out
    #@timeout 2
  done	
  echo >> saidasMPI3/saidaMPI3.out
done
export KMP_AFFINITY=scatter
for t in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    ./daxpy.exe $t $s $t >> saidasOMP/saidaOMP.out
    #@timeout 2
  done
  echo >> saidasOMP/saidaOMP.out
done

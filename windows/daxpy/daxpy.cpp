#include <omp.h>
#include <windows.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;

int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); //3200000000
	long long int domSize     = vecSize/nDomains;
	double alpha = 3.141592;

	int num_threads = atoi(argv[3]);
	
	// _PRINT( nDomains )
	// _PRINT( vecSize )
	// _PRINT( domSize )
	// _PRINT( num_threads )

	//Alocação pela thread master
	double** a = (double**) malloc(nDomains*sizeof(double*));
	double** b = (double**) malloc(nDomains*sizeof(double*));
	
	double* elapsedTimes = new double[ nDomains ];

	//Início da região paralela
	#pragma omp parallel num_threads(num_threads) shared(elapsedTimes)
	{			
		const int threadId = omp_get_thread_num();

		double* dataA[nDomains];
		double* dataB[nDomains];
		
		#pragma omp single copyprivate(dataA,dataB)
		for ( int dom = 0; dom < nDomains; ++dom )
		{
			dataA[dom] = (double*) malloc(domSize*sizeof(double));
			dataB[dom] = (double*) malloc(domSize*sizeof(double));
		}
		
		std::fill_n( dataA[threadId], domSize, 2.0 );
		std::fill_n( dataB[threadId], domSize, 1.0 );
		
		double* domInA = dataA[threadId];
		double* domInB = dataB[threadId];
		
			
		LARGE_INTEGER frequency;
		LARGE_INTEGER t1, t2;

		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&t1);		
		
		for(int i=0; i<200; i++)
		for( long long int elem=0; elem<domSize; elem++ ){
			domInA[elem] = alpha*domInA[elem] + domInB[elem];								
		}			
		
		QueryPerformanceCounter(&t2);	
		elapsedTimes[ threadId ] = static_cast<double>( t2.QuadPart - t1.QuadPart ) / static_cast<double>( frequency.QuadPart );	
		
		a[threadId] = domInA;
		b[threadId] = domInB;		
	}

	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(elapsedTimes[i] > max)
			max = elapsedTimes[i];		
	}
	std::wcout << max << ", ";
		
	//Imprimindo o vetor A atualizado após a parte paralela
	double somaA = 0.0;
	for (long long int dom=0; dom<nDomains; dom++){
		for(long long int elem=0; elem<domSize; elem++){
			somaA += a[dom][elem];
			if(vecSize<=16){
				//printf("a[%d][%d]: %.4f\n", dom, elem, a[dom][elem]); 			
			}
		}		
	}
	//printf(" Somatorio do vetor A: %.4f\n", somaA);	
	
	delete [] elapsedTimes;
	
}
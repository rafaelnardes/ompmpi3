#include <omp.h>
#include <string.h>
#include <iostream>
#include <stdio.h>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;

int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]);
	long long int matSize     = vecSize*vecSize;
	
	long long int domVecSize     = vecSize/nDomains;
	long long int domMatSize     = matSize/nDomains; //Not a square matrix

	int num_threads = atoi(argv[3]);

	double** a[nDomains];
	double* b[nDomains];
	double* x[nDomains];
	
	double* elapsedTimes = new double[ nDomains ];

	//Início da região paralela
	#pragma omp parallel num_threads(num_threads) shared(elapsedTimes)
	{			
		const int threadId = omp_get_thread_num();

		double** dataA[nDomains];
		double*  datab[nDomains];
		double*  datax[nDomains];
		
		#pragma omp single copyprivate(dataA,datab,datax)
		for ( int dom = 0; dom < nDomains; ++dom )
		{
			dataA[dom] = (double**) malloc(domVecSize*sizeof(double*));			
			for(long long int i=0; i<domVecSize; i++) 
				dataA[dom][i]=(double *) malloc(vecSize*sizeof(double));
		
			datab[dom] = (double*) malloc(domVecSize*sizeof(double));
			datax[dom] = (double*) malloc(domVecSize*sizeof(double));
		}
		
		for ( long long int i = 0; i < domVecSize; i++ )
			std::fill_n( dataA[threadId][i], vecSize, 2.0 );
		std::fill_n( datab[threadId], domVecSize, 0.0 );
		std::fill_n( datax[threadId], domVecSize, 3.0 );

		for(long long int i=0; i<domVecSize; i++){
			*(datax[threadId]+i) = (double) threadId*domVecSize+i+1;	
			//printf("%.0f, ",*(datax[threadId]+i));
		}
		
		#pragma omp barrier
		
		double** domInA = dataA[threadId];
		double* domInb = datab[threadId];
		double* domInx = datax[threadId];

		double start, end;
		start = omp_get_wtime();			

		double aux;
		
		for(int i=0; i<200; i++){
			for( long long int itElemb=0; itElemb<domVecSize; itElemb++ ){ //Para cada elemento a ser calculado dentro de um domínio em b
				aux = 0.0;					
				for(long long int itDomx=0; itDomx<nDomains; itDomx++){
					for(long long int itElemx=0; itElemx<domVecSize; itElemx++){
						aux += (domInA[itElemb][itDomx*domVecSize+itElemx]*datax[itDomx][itElemx]);						
					}		
				}
				domInb[itElemb] = aux;
			}			
		}
		end = omp_get_wtime();
		elapsedTimes[ threadId ] = end-start;	
		#pragma omp barrier		
		a[threadId] = domInA;
		b[threadId] = domInb;
		x[threadId] = domInx;
	}

	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(elapsedTimes[i] > max)
			max = elapsedTimes[i];		
	}
	printf("%f, ", max);
 		
	//Imprimindo o vetor b após a parte paralela
	double somaB = 0.0;
	for (long long int dom=0; dom<nDomains; dom++){
		for(long long int elem=0; elem<domVecSize; elem++){
			somaB += b[dom][elem];
			if(vecSize<=16)
				printf("\nb[%d][%d]: %.0f", dom, elem, b[dom][elem]); 				
				//printf("x[%d][%d]: %.0f\n", dom, elem, x[dom][elem]);
				//for(long long int j = 0; j<vecSize; j++)	
					//printf("a[%d][%d][%d]: %.0f\n", dom, elem, j, a[dom][elem][j]);
		}		
	}
	
    	//printf(" Somatorio do vetor b: %.0f\n", somaB);	
		
	delete [] elapsedTimes;
	
}

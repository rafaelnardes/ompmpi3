#include "E:\SolverBR\ThirdParty\mpi\intel64\include\windows\mpi.h"
#include <omp.h>
#include <windows.h>
#include <iostream>
	
int main (int argc, char * argv[])
{	
	long long int size        = atoll(argv[1]); 
	
	int rank, nProcs;
	long long int mySize;
	MPI_Win winA;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
	
	mySize  = size/nProcs;
			
	float* myPtrA;	
	
	MPI_Win_allocate_shared( mySize*sizeof(float), sizeof(float), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrA, &winA );	
	
	std::fill_n( myPtrA, mySize, 2.0 );

	MPI_Barrier(MPI_COMM_WORLD);
	
	if(rank==0){
		printf(" Allocated. \n");			
	}
		
	MPI_Win_free (&winA);
	MPI_Finalize();
	
	}
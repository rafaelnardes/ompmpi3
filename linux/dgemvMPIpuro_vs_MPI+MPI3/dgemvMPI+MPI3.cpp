#include <mpi.h>
#include <omp.h>
#include <string.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;
#define _PRINT2( x, y ) std::wcout << #x " = " << x << " " #y " = " << y << std::endl;
#define _PRINT3( x, y, z) std::wcout << #x " = " << x << " " #y " = " << y << " " #z " = " << z << std::endl;
	
int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); 
	long long int domSize     = vecSize/nDomains;

	long long int matSize     = vecSize*vecSize;
	long long int domVecSize  = vecSize/nDomains;
	long long int domMatSize  = matSize/nDomains; //Not a square matrix

	int nSocks = 2; //Simulação em uma máquina com 2 sockets.
	if(nDomains<nSocks){
		nSocks=nDomains;
	}
	long long int nDomsPerSock = nDomains/nSocks;
		
	FILE * pFile;
	int world_size, world_rank;
	MPI_Win winA, winB, winX, winVecXBuffer, winTimes;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	
	// Get the group of processes in MPI_COMM_WORLD
	MPI_Group worldGroup;
	MPI_Comm_group(MPI_COMM_WORLD, &worldGroup);
	
	MPI_Group headGroup;
	int* headRanks = (int*) malloc(nSocks*sizeof(int));
	int pos=0;
	for(int rank=0; rank<world_size; rank++){
		//Ao usar pinagem tipo allcores, trocar linha abaixo por: if(rank%nDomsPerSock==0){
		if(rank<nSocks){ //Isto é adequado à pinagem tipo scatter.
			headRanks[pos]=rank;
			pos++;			
		}
	}
	//exit(0);
	
	MPI_Group_incl(worldGroup, nSocks, headRanks, &headGroup);
	
	MPI_Comm head_comm;
	MPI_Comm_create(MPI_COMM_WORLD, headGroup, &head_comm);
	
	// for(int i =0; i<nSocks; i++)
		// printf(" HEAD[%d]= %d\n", i, headRanks[i]);
	// exit(0);
	
	// double* b[nDomains];
	// std::fill_n( b, nDomains, (double *) malloc(domVecSize*sizeof(double)) );
	
	double* b = (double*) malloc( vecSize * sizeof(double) );
	
	double* myPtrA;	
	double*	myPtrB;
	double* myPtrX;
	double* myPtrAuxX;
	double* myPtrVecXBuffer;
		
	MPI_Comm socket_comm;

	//Ao usar pinagem tipo allcores, trocar linha abaixo por: int color = world_rank/(world_size/nSocks);
	int color = world_rank%nSocks; //Isto é adequado à pinagem tipo scatter. (Colors: 01010101)
	
	MPI_Comm_split(MPI_COMM_WORLD, color, world_rank, &socket_comm);
	MPI_Barrier(MPI_COMM_WORLD);
	int sock_rank, sock_size;
	MPI_Comm_rank(socket_comm, &sock_rank);
	MPI_Comm_size(socket_comm, &sock_size);

	MPI_Win_allocate_shared( domMatSize*sizeof(double), sizeof(double), MPI_INFO_NULL, socket_comm, &myPtrA, &winA );
	MPI_Win_allocate_shared( domVecSize*sizeof(double), sizeof(double), MPI_INFO_NULL, socket_comm, &myPtrB, &winB );
	MPI_Win_allocate_shared( domVecSize*sizeof(double), sizeof(double), MPI_INFO_NULL, socket_comm, &myPtrX, &winX );
	MPI_Win_allocate_shared( (vecSize/nDomsPerSock)*sizeof(double), sizeof(double), MPI_INFO_NULL, socket_comm, &myPtrVecXBuffer, &winVecXBuffer );
			
	std::fill_n( myPtrA, domMatSize, 2.0 );
	std::fill_n( myPtrB, domVecSize, 0.0 );
	std::fill_n( myPtrX, domVecSize, 3.0 );
		
	for(long long int i = 0; i< domVecSize; i++){
		myPtrX[i] = world_rank*domVecSize+i+1;	
	}
		
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispUnit;     // Unidade de displacement local
	//Atencão! Estes "base pointers" são para o trecho dentro de cada socket, pois refeenciam janelas criadas com comunicadores de socket		
	double* baseX;
	double* baseVecXBuffer;
	MPI_Win_shared_query(winX, MPI_PROC_NULL, &mySize, &dispUnit, &baseX);
	MPI_Win_shared_query(winVecXBuffer, MPI_PROC_NULL, &mySize, &dispUnit, &baseVecXBuffer);
	
	double** domInA;
	double* domInb;
	double* domInx;		
	domInA = (double**) malloc(domVecSize*sizeof(double*));			
	for(long long int i=0; i<domVecSize; i++) 
		domInA[i] = &myPtrA[i*vecSize];
	domInb = myPtrB;
	domInx = myPtrX;
			
	double* myPtrElapsedTime;
	MPI_Win_allocate_shared( 1*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrElapsedTime, &winTimes );
	double start, end;
	int nVezes=1;
	double max,total;
	max=0.0;
	total=0.0;
	
	for(int vez=1; vez<=nVezes; vez++){ //Laço para tomada da média do tempo de execução em nVezes repetições

	start = MPI_Wtime();

	memcpy( &baseVecXBuffer[domVecSize*world_rank], domInx, domVecSize*sizeof(double) );
        MPI_Barrier(MPI_COMM_WORLD);

        //for(long long int i = 0; i<vecSize; i++){
              //if(world_rank==0)printf("xANTES[%d]=%.0f \n", i, baseVecXBuffer[i]);                   
        //}
        //MPI_Barrier(MPI_COMM_WORLD);
        //exit(0);

	for(long long int itSock=0; itSock<nSocks; itSock++){
		for(long long int itDomx=0; itDomx<nDomsPerSock; itDomx++){	   
			if(sock_rank==0){
				MPI_Bcast(&baseVecXBuffer[domVecSize*itDomx*nSocks+itSock*domVecSize], domVecSize, MPI_DOUBLE, itSock, head_comm); 
				//printf("WR=%d, i's=%d,%d,%d  [%.0f,%.0f,%.0f]\n", world_rank, domVecSize*itDomx*nSocks+itSock*domVecSize, domVecSize*itDomx*nSocks+itSock*domVecSize+1, domVecSize*itDomx*nSocks+itSock*domVecSize+2, baseVecXBuffer[domVecSize*itDomx*nSocks], baseVecXBuffer[domVecSize*itDomx*nSocks+1], baseVecXBuffer[domVecSize*itDomx*nSocks+2]);
			}
		}
	}
	
	//MPI_Barrier(MPI_COMM_WORLD);
	//for(long long int i = 0; i<vecSize; i++){
	//	if(world_rank==0)printf("xDEPOIS[%d]=%.0f \n", i, baseVecXBuffer[i]); 			
	//}
	//MPI_Barrier(MPI_COMM_WORLD);
	//exit(0);
	
	//Cálculo
	double aux;
	for(int i=0; i<200; i++){
		for(long long int itElemb=0; itElemb<domVecSize; itElemb++ ){ //Para cada elemento a ser calculado dentro de um domínio em b
			aux = 0.0;
			for(long long int itDomx=0; itDomx<nDomains; itDomx++){ //para cada dominio itDomx em X				
				for(long long int itElemx=0; itElemx<domVecSize; itElemx++){ 
					aux += ( domInA[itElemb][itDomx*domVecSize+itElemx] * baseVecXBuffer[itDomx*domVecSize+itElemx] );							
				} 
			} 
			domInb[itElemb] = aux;			
		} 
	}
	MPI_Gather(domInb, domVecSize, MPI_DOUBLE, b, domVecSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		
	end = MPI_Wtime();
	*myPtrElapsedTime = end - start;
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize3;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispunit3;     // Unidade de displacement local
	double* baseElapsedTimes;
	MPI_Win_shared_query(winTimes, MPI_PROC_NULL, &mySize3, &dispunit3, &baseElapsedTimes);
	
	MPI_Barrier(MPI_COMM_WORLD);
	max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(baseElapsedTimes[i] > max)
			max = baseElapsedTimes[i];
	}
	total += max;
	} 
	
	MPI_Barrier(MPI_COMM_WORLD);
	max=total/nVezes;
		
	double somaB = 0.0;
	for(long long int i = 0; i<vecSize; i++){
		somaB += b[i];
			if(vecSize<=16 && world_rank==0){
				printf("b[%d]=%.0f \n", i, b[i]); 			
			}					
	}
			
	if(world_rank==0){
		printf("%f, ", max);
		//printf(" Somatorio do vetor b: %.0f\n", somaB);				
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Win_free (&winA);
	MPI_Win_free (&winB);
	MPI_Win_free (&winX);
	MPI_Finalize();
		
	}

#!/bin/bash
rm saidasMPI3/*
rm saidasOMP/*
unset I_MPI_PIN_PROCESSOR_LIST
for p in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    mpirun -n $p ./arraySumMPI3.exe $p $s >> saidasMPI3/saidaMPI3NoPinning.out
    #@timeout 2
  done
  echo >> saidasMPI3/saidaMPI3NoPinning.out
done
export I_MPI_PIN_PROCESSOR_LIST=allcores
for p in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    mpirun -n $p ./arraySumMPI3.exe $p $s >> saidasMPI3/saidaMPI3allcores.out
    #@timeout 2
  done
  echo >> saidasMPI3/saidaMPI3allcores.out
done
export I_MPI_PIN_PROCESSOR_LIST="0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15"
for p in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    mpirun -n $p ./arraySumMPI3.exe $p $s >> saidasMPI3/saidaMPI3Scatter.out
    #@timeout 2
  done
  echo >> saidasMPI3/saidaMPI3Scatter.out
done
export KMP_AFFINITY=compact
for t in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    ./arraySumNU.exe $t $s $t >> saidasOMP/saidaOMPCompactNU.out
    #@timeout 2
  done
  echo >> saidasOMP/saidaOMPCompactNU.out
done
for t in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    ./arraySumAPSingleIP.exe $t $s $t >> saidasOMP/saidaOMPCompactAPSingleIP.out
    #@timeout 2
  done
  echo >> saidasOMP/saidaOMPCompactAPSingleIP.out
done
export KMP_AFFINITY=Scatter
for t in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    ./arraySumNU.exe $t $s $t >> saidasOMP/saidaOMPScatterNU.out
    #@timeout 2
  done
  echo >> saidasOMP/saidaOMPScatterNU.out
done
for t in 1 2 4 6 8 10 12 14 16
do
  for s in 6881280 13762560 27525120 55050240 110100480
  do
    ./arraySumAPSingleIP.exe $t $s $t >> saidasOMP/saidaOMPScatterAPSingleIP.out
    #@timeout 2
  done
  echo >> saidasOMP/saidaOMPScatterAPSingleIP.out
done

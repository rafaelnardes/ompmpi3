del *.exe
del *.obj

icl arraySumNU.cpp /Qopenmp /Qstd=c++11 /O2
icl arraySumAPIP.cpp /Qopenmp /Qstd=c++11 /O2
icl arraySumASIP.cpp /Qopenmp /Qstd=c++11 /O2
icl arraySumAPSingleIP.cpp /Qopenmp /Qstd=c++11 /O2

mpiiCC arraySumMPI3.cpp /O2
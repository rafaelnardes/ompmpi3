#include <mpi.h>
#include <omp.h>
#include <string.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;
	
int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); 
	long long int domSize     = vecSize/nDomains;

	long long int matSize     = vecSize*vecSize;
	long long int domVecSize  = vecSize/nDomains;
	long long int domMatSize  = matSize/nDomains; //Not a square matrix
		
	FILE * pFile;
	int comm_size, rank;
	MPI_Win winA, winB, winX, winTimes;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
			
	double* myPtrA;	
	double*	myPtrB;
	double* myPtrX;
	//Cada processo alocará 1 domain
	
	//MPI_Info info;
	//MPI_Info_create(&info);
	//MPI_Info_set(info, "alloc_shared_noncontig", "true");
	
	MPI_Win_allocate_shared( domMatSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrA, &winA );
	MPI_Win_allocate_shared( domVecSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrB, &winB );
	MPI_Win_allocate_shared( domVecSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrX, &winX );
	
	//MPI_Win_allocate_shared( domMatSize*sizeof(double), sizeof(double), info, MPI_COMM_WORLD, &myPtrA, &winA );
	
	std::fill_n( myPtrA, domMatSize, 2.0 );
	std::fill_n( myPtrB, domVecSize, 0.0 );
	std::fill_n( myPtrX, domVecSize, 3.0 );
	
	for(long long int i = 0; i< domVecSize; i++){
		myPtrX[i] = rank*domVecSize+i+1;
	}
		
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispUnit;     // Unidade de displacement local
	
    double* baseB;
	double* baseX; 
	MPI_Win_shared_query(winB, MPI_PROC_NULL, &mySize, &dispUnit, &baseB);
	MPI_Win_shared_query(winX, MPI_PROC_NULL, &mySize, &dispUnit, &baseX);
	
	double* myPtrElapsedTime;
	MPI_Win_allocate_shared( 1*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrElapsedTime, &winTimes );

	double start, end;	

	start = MPI_Wtime();
	
	double aux;
	
	//Cálculo
	for(int i=0; i<200; i++){
		for( long long int elem=0; elem<domVecSize; elem++ ){
			aux = 0.0;
			for(long long int j=0; j<vecSize; j++){
				aux += (myPtrA[elem*vecSize+j] * baseX[j]);				
			}
			myPtrB[elem] = aux;
		}
	}

	end = MPI_Wtime();
	*myPtrElapsedTime = end-start;
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize3;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispunit3;     // Unidade de displacement local
	double* baseElapsedTimes;
	MPI_Win_shared_query(winTimes, MPI_PROC_NULL, &mySize3, &dispunit3, &baseElapsedTimes);
	
	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(baseElapsedTimes[i] > max)
			max = baseElapsedTimes[i];
	}
	
	double somaB = 0.0;
	for (long long int dom=0; dom<nDomains; dom++){
		for(long long int elem=0; elem<domVecSize; elem++){
			somaB += baseB[dom*domVecSize+elem];
			if(vecSize<=16 && rank==0){
				//printf("\nb[%d][%d]: %.0f", dom, elem, baseB[dom*domVecSize+elem]); 			
			}
		}		
	}
		
	if(rank==0){
		printf("%f, ", max);
		//printf(" Somatorio do vetor b: %.0f\n", somaB);			
	}
		
	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Win_free (&winA);
	MPI_Win_free (&winB);
	MPI_Win_free (&winX);
	MPI_Finalize();
	
	}

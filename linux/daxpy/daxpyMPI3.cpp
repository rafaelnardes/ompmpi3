#include <mpi.h>
#include <omp.h>
#include <string.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;
	
int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); //3200000000
	long long int domSize     = vecSize/nDomains;	
	double alpha = 3.141592;
	FILE * pFile;
	int comm_size, rank;
	MPI_Win winA, winB, winTimes;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		
	// if(rank==0){
		// _PRINT( nDomains )
		// _PRINT( vecSize )
		// _PRINT( domSize )
		// _PRINT( comm_size )
	// }
	
	double* myPtrA;	
	double*	myPtrB;

	//Cada processo alocará 1 domain (depois, mudar para um bloco de n domains)
	MPI_Win_allocate_shared( domSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrA, &winA );
	MPI_Win_allocate_shared( domSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrB, &winB );
	
	std::fill_n( myPtrA, domSize, 2.0 );
	std::fill_n( myPtrB, domSize, 1.0 );

	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispunit;     // Unidade de displacement local	
	double* baseA;
    MPI_Win_shared_query(winA, MPI_PROC_NULL, &mySize, &dispunit, &baseA);
		
	double* myPtrElapsedTime;
	MPI_Win_allocate_shared( 1*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrElapsedTime, &winTimes );

	double start, end;	
	start = MPI_Wtime();
	
	for(int i=0; i<200; i++)	
	for( long long int elem=0; elem<domSize; elem++ ) {
		myPtrA[elem] = alpha*myPtrA[elem] + myPtrB[elem];	
	}		
		
	end = MPI_Wtime();
	*myPtrElapsedTime = end-start;	

	MPI_Barrier(MPI_COMM_WORLD);
	
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize2;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispunit2;     // Unidade de displacement local
	double* baseElapsedTimes;
	MPI_Win_shared_query(winTimes, MPI_PROC_NULL, &mySize2, &dispunit2, &baseElapsedTimes);
	
	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(baseElapsedTimes[i] > max)
			max = baseElapsedTimes[i];
	}		
	
	double somaA = 0.0;
	for (long long int dom=0; dom<nDomains; dom++){
		for(long long int elem=0; elem<domSize; elem++){
			somaA += baseA[dom*domSize+elem];
			if(vecSize<=16 && rank==0){
				//printf("a[%d][%d]: %.4f\n", dom, elem, baseA[dom*domSize+elem]); 			
			}
		}		
	}
	
	if(rank==0){
		printf("%f, ", max);
		//std::wcout << "MyTime: " << baseElapsedTimes[rank] << ", " ;
		//printf(" Somatorio do vetor A: %.4f\n", somaA);			
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Win_free (&winA);
	MPI_Win_free (&winB);
	MPI_Finalize();
	
	}

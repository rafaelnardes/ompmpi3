# include "E:\SolverBR\ThirdParty\mpi\intel64\include\windows\mpi.h"
#include <omp.h>
#include <windows.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;
	
int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); //3200000000
	long long int domSize     = vecSize/nDomains;
	//int num_threads = atoi(argv[3]);
	FILE * pFile;
	int comm_size, rank;
	MPI_Win winA, winB, winC, winTimes;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		
	// if(rank==0){
		// _PRINT( nDomains )
		// _PRINT( vecSize )
		// _PRINT( domSize )
		// _PRINT( comm_size )
	// }
	
	//long long int myNdomains = nDomains/comm_size;
	
	double* myPtrA;	
	double*	myPtrB;
	double* myPtrC;
	//Cada processo alocará 1 domain (depois, mudar para um bloco de n domains)
	MPI_Win_allocate_shared( domSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrA, &winA );
	MPI_Win_allocate_shared( domSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrB, &winB );
	MPI_Win_allocate_shared( domSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrC, &winC );
	std::fill_n( myPtrA, domSize, 1.0 );
	std::fill_n( myPtrB, domSize, 2.0 );
	std::fill_n( myPtrC, domSize, 0.0 );
	
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispUnit;     // Unidade de displacement local
	double* baseA;
    double* baseB;
	double* baseC;
	MPI_Win_shared_query(winA, MPI_PROC_NULL, &mySize, &dispUnit, &baseA);
	MPI_Win_shared_query(winB, MPI_PROC_NULL, &mySize, &dispUnit, &baseB);
	MPI_Win_shared_query(winC, MPI_PROC_NULL, &mySize, &dispUnit, &baseC);	
	
	// if(false){
		// printf("Sou o rank %d. Valores em A: \n", rank);	
		// for(int i=0; i<vecSize; i++){
			// printf("a[%d]=%.0f,  ", i, baseA[i]);					
		// }
		// printf("\n");
		// for(int i=0; i<vecSize; i++){
			// printf("b[%d]=%.0f,  ", i, baseB[i]);			
		// }
		// printf("\n");
		// for(int i=0; i<vecSize; i++){
			// printf("c[%d]=%.0f,  ", i, baseC[i]);			
		// }
		// printf("\n");
	// }
	
	// MPI_Barrier(MPI_COMM_WORLD);
	
	double* myPtrElapsedTime;
	MPI_Win_allocate_shared( 1*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrElapsedTime, &winTimes );
	LARGE_INTEGER frequency;
	LARGE_INTEGER t1, t2;
	
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);
	
	for(int i=0; i<200; i++)	
	for( long long int elem=0; elem<domSize; elem++ ) {
		myPtrC[elem] = myPtrB[elem] + myPtrA[elem];	
	}		
		
	QueryPerformanceCounter(&t2);
	*myPtrElapsedTime = static_cast<double>( t2.QuadPart - t1.QuadPart ) / static_cast<double>( frequency.QuadPart );
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize2;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispUnit2;     // Unidade de displacement local
	double* baseElapsedTimes;
	MPI_Win_shared_query(winTimes, MPI_PROC_NULL, &mySize2, &dispUnit2, &baseElapsedTimes);
	
	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {			
		if(baseElapsedTimes[i] > max)
			max = baseElapsedTimes[i];
	}
	
	double somaC = 0.0;
	for (long long int dom=0; dom<nDomains; dom++){
		for(long long int elem=0; elem<domSize; elem++){
			somaC += baseC[dom*domSize+elem];
			if(vecSize<=16 && rank==0){
				//printf("c[%d][%d]: %.0f\n", dom, elem, baseC[dom*domSize+elem]); 			
			}
		}		
	}
	
	if(rank==0){
		std::wcout << max << ", ";
		//printf(" Somatorio do vetor C: %.0f\n", somaC);			
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Win_free (&winA);
	MPI_Win_free (&winB);
	MPI_Win_free (&winC);
	MPI_Finalize();
	
	}
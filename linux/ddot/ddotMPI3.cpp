#include <mpi.h>
#include <omp.h>
#include <string.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;
	
int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); 
	long long int domSize     = vecSize/nDomains;
		
	FILE * pFile;
	int comm_size, rank;
	MPI_Win winA, winB, winLocalSums, winTimes;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		
	double* myPtrA;	
	double*	myPtrB;
	
	//Cada processo alocará 1 domain (depois, mudar para um bloco de n domains)
	MPI_Win_allocate_shared( domSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrA, &winA );
	MPI_Win_allocate_shared( domSize*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrB, &winB );
		
	std::fill_n( myPtrA, domSize, 2.0 );
	std::fill_n( myPtrB, domSize, 3.0 );
		
	// Argumentos para saída de MPI_Win_shared_query
	// MPI_Aint mySize;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	// int dispUnit;     // Unidade de displacement local
	// double* baseA;
    // double* baseB;
	// MPI_Win_shared_query(winA, MPI_PROC_NULL, &mySize, &dispUnit, &baseA);
	// MPI_Win_shared_query(winB, MPI_PROC_NULL, &mySize, &dispUnit, &baseB);
	
	double* myPtrElapsedTime;
	double* myPtrLocalSums;
	
	MPI_Win_allocate_shared( 1*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrElapsedTime, &winTimes );
	MPI_Win_allocate_shared( 1*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrLocalSums, &winLocalSums );
	*myPtrLocalSums = 0.0;
	
	double start, end;	
	start = MPI_Wtime();
	
	double mySum;
	for(int i=0; i<200; i++){
		mySum=0.0;	
		for( long long int elem=0; elem<domSize; elem++ ) {
			//*myPtrLocalSums += myPtrA[elem] * myPtrB[elem];	
			mySum += myPtrA[elem] * myPtrB[elem];	
		}
	}
	*myPtrLocalSums = mySum;

	MPI_Barrier(MPI_COMM_WORLD);
	
	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispUnit;     // Unidade de displacement local
	double* baseElapsedTimes;
	double* baseLocalSums;
	MPI_Win_shared_query(winTimes, MPI_PROC_NULL, &mySize, &dispUnit, &baseElapsedTimes);
	MPI_Win_shared_query(winLocalSums, MPI_PROC_NULL, &mySize, &dispUnit, &baseLocalSums);
		
	double dotp = 0.0;
	for (long long int dom=0; dom<nDomains; dom++){
		dotp += baseLocalSums[dom];	
	}

	end = MPI_Wtime();
	*myPtrElapsedTime = end-start;

	MPI_Barrier(MPI_COMM_WORLD);

	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(baseElapsedTimes[i] > max)
			max = baseElapsedTimes[i];
	}
	
	if(rank==0){
		printf("%f, ", max);	
		//std::wcout << "MyTime: " << baseElapsedTimes[rank] << ", " ;
		//printf(" Dot Product: %.4f\n", dotp);		
	}
		
	MPI_Barrier(MPI_COMM_WORLD);
		
	MPI_Win_free (&winA);
	MPI_Win_free (&winB);
	MPI_Finalize();
	
	}

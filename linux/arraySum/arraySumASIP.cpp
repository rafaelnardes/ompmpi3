#include <omp.h>
#include <string.h>
#include <iostream>
#include <stdio.h>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;

int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); //3200000000
	long long int domSize     = vecSize/nDomains;

	int num_threads = atoi(argv[3]);
	
	// _PRINT( nDomains )
	// _PRINT( vecSize )
	// _PRINT( domSize )
	// _PRINT( num_threads )

	double** a = (double**) malloc(nDomains*sizeof(double*));
	double** b = (double**) malloc(nDomains*sizeof(double*));
	double** c = (double**) malloc(nDomains*sizeof(double*));
	
	for(long long int i=0; i<nDomains; i++){ 
		a[i] = (double *) malloc(domSize*sizeof(double));
		b[i] = (double *) malloc(domSize*sizeof(double));
		c[i] = (double *) malloc(domSize*sizeof(double));
	}
	
	double* elapsedTimes = new double[ nDomains ];

	//Início da região paralela
	#pragma omp parallel num_threads(num_threads) shared(elapsedTimes)
	{			
		const int threadId = omp_get_thread_num();
		
		double* domInA = a[threadId];
		double* domInB = b[threadId];
		double* domInC = c[threadId];
		std::fill_n( domInA, domSize, 1.0 );
		std::fill_n( domInB, domSize, 2.0 );
		std::fill_n( domInC, domSize, 0.0 );

		double start, end;
		start = omp_get_wtime();	

		for(int i=0; i<200; i++)
		for( long long int elem=0; elem<domSize; elem++ ){
			domInC[elem] = domInB[elem] + domInA[elem];								
		}			
		
		end = omp_get_wtime();
		elapsedTimes[ threadId ] = end-start;		
		// #pragma omp barrier
		// a[threadId] = domInA;
		// b[threadId] = domInB;
		// c[threadId] = domInC;
	}

	double soma = 0.0;
	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		//std::wcout << "Thread " << i << " executou em " << elapsedTimes[ i ] <<L" sec"<<std::endl;
		soma += elapsedTimes[ i ];
		if(elapsedTimes[i] > max)
			max = elapsedTimes[i];		
	}
	//std::wcout << "===> Media de tempo das threads: " << soma/num_threads <<L" sec"<<std::endl;
	//std::wcout << "===> Tempo máximo de 1 thread  : " << max <<L" sec"<<std::endl;
	printf("%f, ", max);
		
	//Imprimindo o vetor C após a parte paralela
	double total = 0.0;
	for (long long int dom=0; dom<nDomains; dom++){
		for(long long int elem=0; elem<domSize; elem++){
			total += c[dom][elem];
			if(vecSize<=16)
				printf("c[%d][%d]: %.0f\n", dom, elem, c[dom][elem]); 			
		}		
	}
	//printf(" Somatorio do vetor C: %.0f\n", total);	
	
	delete [] elapsedTimes;
	
}

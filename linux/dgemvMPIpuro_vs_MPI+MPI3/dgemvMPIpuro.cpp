#include <mpi.h>
#include <omp.h>
#include <string.h>
#include <iostream>

#define _PRINT( x ) std::wcout << #x " = " << x << std::endl;
	
int main (int argc, char * argv[])
{	
	long long int nDomains    = atoll(argv[1]);
	long long int vecSize     = atoll(argv[2]); 
	long long int domSize     = vecSize/nDomains;

	long long int matSize     = vecSize*vecSize;
	long long int domVecSize  = vecSize/nDomains;
	long long int domMatSize  = matSize/nDomains; //Not a square matrix
		
	FILE * pFile;
	int comm_size, rank;
	MPI_Win winTimes;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	double* b = (double*) malloc(vecSize*sizeof(double));
	
	double** domInA;
	double* domInb;
	double* domInx;
	//double* auxDomBuffer;
	double* vecXBuffer;

	domInA = (double**) malloc(domVecSize*sizeof(double*));			
	for(long long int i=0; i<domVecSize; i++) 
		domInA[i]=(double *) malloc(vecSize*sizeof(double));

	domInb = (double*) malloc(domVecSize*sizeof(double));
	domInx = (double*) malloc(domVecSize*sizeof(double));	
	//auxDomBuffer = (double*) malloc(domVecSize*sizeof(double));
	vecXBuffer = (double*) malloc(vecSize*sizeof(double));
	
	for ( long long int i = 0; i < domVecSize; i++ )
		std::fill_n( domInA[i], vecSize, 2.0 );
	std::fill_n( domInb, domVecSize, 0.0 );
	std::fill_n( domInx, domVecSize, 3.0 ); 
	
	for(long long int i = 0; i< domVecSize; i++){
		domInx[i] = rank*domVecSize+i+1;
	}

	double* myPtrElapsedTime;
	MPI_Win_allocate_shared( 1*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &myPtrElapsedTime, &winTimes );
	
	double start, end;
	
	start = MPI_Wtime();
	

	memcpy(&vecXBuffer[domVecSize*rank], domInx, domVecSize*sizeof(double) );
	for(long long int itDomx=0; itDomx<nDomains; itDomx++){
		//if(rank==itDomx)
		//memcpy(&vecXBuffer[domVecSize*itDomx], domInx, domVecSize*sizeof(double) );	
		MPI_Bcast(&vecXBuffer[domVecSize*itDomx], domVecSize, MPI_DOUBLE, itDomx, MPI_COMM_WORLD);												
	}
	//MPI_Barrier(MPI_COMM_WORLD);
	//printf("Imprimindo vetor X inteiro: ");
	//for(long long int i = 0; i<vecSize; i++){
	//	printf("x[%d]=%.0f \n", i, vecXBuffer[i]); 			
	//}
	//MPI_Barrier(MPI_COMM_WORLD);
	//exit(0);

		
	//Cálculo
	double aux;		
	for(int i=0; i<200; i++){			
		for( long long int itElemb=0; itElemb<domVecSize; itElemb++ ){ //Para cada elemento a ser calculado dentro de um domínio em b
			aux = 0.0;						
			for(long long int itDomx=0; itDomx<nDomains; itDomx++){ //para cada dominio itDomx em X
				//memcpy(auxDomBuffer, domInx, domVecSize*sizeof(double) );				
				//MPI_Bcast(auxDomBuffer, domVecSize, MPI_DOUBLE,itDomx,MPI_COMM_WORLD);												
				for(long long int itElemx=0; itElemx<domVecSize; itElemx++){								
					//aux += (domInA[itElemb][itDomx*domVecSize+itElemx]*auxDomBuffer[itElemx]);
					aux += (domInA[itElemb][itDomx*domVecSize+itElemx]*vecXBuffer[domVecSize*itDomx+itElemx]);
				}		
			}
			domInb[itElemb] = aux;
		}			
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Gather(domInb, domVecSize, MPI_DOUBLE, b, domVecSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	
	end = MPI_Wtime();
	*myPtrElapsedTime = end-start;

	MPI_Barrier(MPI_COMM_WORLD);

	// Argumentos para saída de MPI_Win_shared_query
	MPI_Aint mySize3;  // Receberá o tamanho do pedaço de shared memory que está em cada processo
	int dispunit3;     // Unidade de displacement local
	double* baseElapsedTimes;
	MPI_Win_shared_query(winTimes, MPI_PROC_NULL, &mySize3, &dispunit3, &baseElapsedTimes);
	
	
	double max = 0.0;
	for(long long int i=0; i<nDomains; i++) {
		if(baseElapsedTimes[i] > max)
			max = baseElapsedTimes[i];
	}
	
	double somaB = 0.0;
	for(long long int i = 0; i<vecSize; i++){
		somaB += b[i];
			if(vecSize<=16 && rank==0){
				printf("b[%d]=%.0f \n", i, b[i]); 			
			}					
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	if(rank==0){
		printf("%f, ", max);
		//printf(" Somatorio do vetor b: %.0f\n", somaB);			
	}
	MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Finalize();
	
	}

del saidasMPI3\saida*
del saidasOMP\saida*

@set I_MPI_PIN_PROCESSOR_LIST=0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15

FOR %%p IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (1680 3360 6720 13440) DO (

		mpiexec -n %%p dgemvMPI3.exe %%p %%s >> E:\testes_MPI\dgemv\saidasMPI3\saidaMPI3.out
		@timeout 2
	)
	ECHO. >> E:\testes_MPI\dgemv\saidasMPI3\saidaMPI3.out
)

@set KMP_AFFINITY=scatter

FOR %%t IN (1 2 4 6 8 10 12 14 16) DO (
	FOR %%s IN (1680 3360 6720 13440) DO (
		dgemv.exe %%t %%s %%t >> E:\testes_MPI\dgemv\saidasOMP\saidaOMP.out
		@timeout 2
	)
	ECHO.>>	E:\testes_MPI\dgemv\saidasOMP\saidaOMP.out
)